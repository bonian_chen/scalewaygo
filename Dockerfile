# FROM golang:bullseye
FROM docker:dind

RUN apk update && apk add git curl jq findutils lz4 brotli ninja-build expect-dev inotify-tools gnupg s3cmd

# RUN --mount=type=cache,target=/var/cache/apt \
#    apt-get update && apt-get install -y curl jq lz4 brotli ninja-build expect-dev inotify-tools gnupg gnupg2 nodejs npm
# RUN apk update && apk add git curl jq lz4 brotli ninja-build expect-dev inotify-tools gnupg nodejs npm
# RUN curl -L https://go.dev/dl/go1.20.5.linux-amd64.tar.gz | tar -C /usr/local -xz
# RUN npm install --package-lock-only && npm install -g serverless
# RUN npm install --package-lock-only && npm install -g serverless && serverless plugin install -n serverless-scaleway-functions
# RUN npm install -g serverless

# RUN curl https://packages.fluentbit.io/fluentbit.key | gpg --dearmor > /usr/share/keyrings/fluentbit-keyring.gpg
# RUN echo 'deb [signed-by=/usr/share/keyrings/fluentbit-keyring.gpg] https://packages.fluentbit.io/debian/bullseye bullseye main' >> /etc/apt/sources.list
# RUN --mount=type=cache,target=/var/cache/apt \
#    apt-get update && apt-get install -y fluent-bit && apt-get clean && rm -rf /var/lib/apt/lists/*;

